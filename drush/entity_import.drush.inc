<?php

/**
 * @file
 * Features module drush integration.
 */

use Drupal\entity_import\EntityImporter;
use Drupal\entity_import\DrushLogger;
use Drush\Log\LogLevel;

/**
 * Implements hook_drush_command().
 */
function entity_import_drush_command() {
  $items = array();

  $items['entity-import'] = array(
    'description' => 'Import entities.',
    'options' => array(
      'dir' => "Directory of the entity files. Defaults to 'sites/default/content'.",
    ),
    'aliases' => array('ei'),
  );
  $items['entity-export'] = array(
    'description' => 'Export entity.',
    'arguments' => array(
      'entity_type' => "The entity type.",
      'id' => "The entity ID.",
    ),
    'options' => array(
      'dir' => "Directory of the entity files. Defaults to 'sites/default/content'.",
    ),
    'aliases' => array('ee'),
  );

  return $items;
}


/**
 * Provides Drush command callback for entity-import.
 */
function drush_entity_import() {

  /** @var EntityImporter $importer */
  $importer = \Drupal::service('entity_import.importer');

  if (drush_get_option('dir')) {
    $importer->setDir(drush_get_option('dir'));
  }

  drush_log("Importing content from '" . $importer->getDir() . "'.", LogLevel::INFO);

  $importer->addSubscriber(new DrushLogger());

  $importer->importEntities();
}

/**
 * Provides Drush command callback for entity-export.
 */
function drush_entity_import_entity_export($entity_type = NULL, $id = NULL) {

  /** @var \Drupal\entity_import\EntityExporter $exporter */
  $exporter = \Drupal::service('entity_import.exporter');

  if (drush_get_option('dir')) {
    $exporter->setDir(drush_get_option('dir'));
  }

  drush_log("Exporting entity to '" . $exporter->getDir() . "'.", LogLevel::INFO);

  $exporter->addSubscriber(new DrushLogger());

  if (is_string($entity_type) && is_numeric($id)) {
    $exporter->exportEntityById($entity_type, $id);
  }
  else {
    $exporter->exportAllEntities($entity_type);
  }
}

