<?php
/**
 * @file
 * Contains \Drupal\entity_import\ContentImporter.
 */

namespace Drupal\entity_import;


use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity;

class EntityImporter extends EntityImportBase implements ContainerInjectionInterface {


  /**
   * Import all entities.
   */
  function importEntities() {

    $files = $this->readFiles();

    foreach ($files as $file) {
      $entityTypeId = strtok($this->fileSystem->basename($file), '.');
      $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
      $data = file_get_contents($file);

      $entity = $this->dataToEntity($data, $entityType->getClass());

      $existing = $this->entityRepository->loadEntityByUuid($entityTypeId, $entity->uuid());
      if ($existing === FALSE) {
        $entity->enforceIsNew();
        $entity->save();

        $this->dispatchImported($entity, $file);
      }
      else {
        // Do not enforce a new entity / new revision.
        $entity->enforceIsNew(FALSE);
        if ($entityType->isRevisionable()) {
          $entity->setNewRevision(FALSE);
        }
        // Apply existing ID.
        $entity->{$entityType->getKey('id')}->value = $existing->id();
        $entity->save();

        $this->dispatchImported($entity, $file, TRUE);
      }
    }
  }

  /**
   * @param ContentEntityInterface $entity
   * @param string $file
   * @param bool $update
   */
  private function dispatchImported($entity, $file, $update = FALSE) {
    $this->dispatcher->dispatch(EntityImportEvent::EVENT_IMPORTED, new EntityImportEvent($entity, $file, $update));
  }

  /**
   * @param string $data
   * @param string $type
   * @return ContentEntityInterface
   */
  private function dataToEntity($data, $type) {
    return $this->serializer->deserialize($data, $type, $this->format);
  }

  /**
   * @return array
   */
  private function readFiles() {
    $fullDir = DRUPAL_ROOT . '/' . $this->dir;
    $d = dir($fullDir);
    $files = glob($fullDir . '/*.' . $this->format);
    $d->close();
    return $files;
  }

}
