<?php

namespace Drupal\entity_import;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityType;

class EntityExporter extends EntityImportBase {

  public function exportAllEntities($type = NULL) {
    if ($type !== NULL) {
      $entities = $this->entityTypeManager->getStorage($type)->loadMultiple();
    }
    else {
      $types = $this->getContentEntityTypes();
      $entities = array();
      foreach ($types as $type) {
        $entities = array_merge($entities, $this->entityTypeManager->getStorage($type)->loadMultiple());
      }
    }

    foreach ($entities as $entity) {
      $this->exportEntity($entity);
    }
  }

  public function exportEntityById($type, $id) {
    $entity = $this->entityTypeManager->getStorage($type)->load($id);

    $this->exportEntity($entity);
  }

  public function exportEntity($entity) {

    if ($entity instanceof ContentEntityInterface) {
      $data = $this->entityToData($entity);
    }
    else {
      throw new \Exception("Could not export entity of type " . get_class($entity) . ". Entity class must implement ContentEntityInterface.");
    }
    $fileName = $entity->getEntityTypeId() . '.' . $entity->uuid();
    if ($this->fileStorage->write($fileName, $data)) {
      $this->dispatchExported($entity, $fileName . '.' . $this->format);
    }
  }

  /**
   * @param ContentEntityInterface $entity
   * @param string $file
   */
  private function dispatchExported($entity, $file) {
    $this->dispatcher->dispatch(EntityImportEvent::EVENT_EXPORTED, new EntityImportEvent($entity, $file));
  }

  /**
   * @param ContentEntityInterface $entity
   * @return scalar
   */
  private function entityToData($entity) {
    return $this->serializer->serialize($entity, $this->format);
  }

  private function getContentEntityTypes() {
    return array_keys(array_filter($this->entityTypeManager->getDefinitions(), function($type) {
      return $type instanceof ContentEntityType;
    }));
  }
}