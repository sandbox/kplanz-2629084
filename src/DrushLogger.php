<?php
/**
 * @file
 * Contains \Drupal\entity_import\DrushLogger.
 */

namespace Drupal\entity_import;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DrushLogger implements EventSubscriberInterface {


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return array(
      EntityImportEvent::EVENT_IMPORTED => array('onImported'),
      EntityImportEvent::EVENT_EXPORTED => array('onExported')
    );
  }

  public function onImported(EntityImportEvent $event) {
    if ($event->isUpdate()) {
      $status = t('Updated entity (' . $event->getEntity()->id() . ').');
    }
    else {
      $status = t('Inserted entity (' . $event->getEntity()->id() . ').');
    }
    drush_print($this->formatFilename($event->getFile()) . ': ' . $status);
  }

  public function onExported(EntityImportEvent $event) {
    $status = t('Exported entity (' . $event->getEntity()->label() . ').');
    drush_print($this->formatFilename($event->getFile()) . ': ' . $status);
  }

  private function formatFilename($filename) {
    return \Drupal::service('file_system')->basename($filename);
  }
}