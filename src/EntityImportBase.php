<?php

namespace Drupal\entity_import;


use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Serializer\Serializer;

class EntityImportBase {

  /**
   * JSON format.
   */
  const FORMAT_JSON = 'json';
  /**
   * XML format.
   */
  const FORMAT_XML = 'xml';

  /**
   * Default import directory.
   */
  const DEFAULT_DIR = 'sites/default/content';
  /**
   * Default file format.
   */
  const DEFAULT_FORMAT = self::FORMAT_JSON;
  /**
   * @var FileSystem
   */
  protected $fileSystem;
  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * @var Serializer
   */
  protected $serializer;
  /**
   * @var string
   */
  protected $format;
  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * ContentImporter constructor.
   *
   * @param FileSystem $fileSystem
   * @param Serializer $serializer
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param EntityRepositoryInterface $entityRepository
   */
  public function __construct(FileSystem $fileSystem, Serializer $serializer, EntityTypeManagerInterface $entityTypeManager, EntityRepositoryInterface $entityRepository) {
    $this->dir = self::DEFAULT_DIR;
    $this->format = self::DEFAULT_FORMAT;

    $this->fileSystem = $fileSystem;
    $this->serializer = $serializer;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityRepository = $entityRepository;

    $this->dispatcher = new EventDispatcher();
    $this->fileStorage = new FileStorage($this->dir, $this->format);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @noinspection PhpParamsInspection */
    return new static(
      $container->get('file_system'),
      $container->get('serializer'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository')
    );
  }

  /**
   * @return string
   */
  public function getDir() {
    return $this->fileStorage->getDirectory();
  }

  /**
   * @param string $dir
   */
  public function setDir($dir) {
    $this->fileStorage->setDirectory($dir);
  }

  /**
   * @param EventSubscriberInterface $subscriber
   */
  function addSubscriber(EventSubscriberInterface $subscriber) {
    $this->dispatcher->addSubscriber($subscriber);
  }
}