<?php
/**
 * @file
 * Contains \Drupal\entity_import\EntityImportEvent.
 */

namespace Drupal\entity_import;


use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\EventDispatcher\Event;

class EntityImportEvent extends Event {

  const EVENT_IMPORTED = 'entity_import.imported';

  const EVENT_EXPORTED = 'entity_import.exported';

  /**
   * @var ContentEntityInterface
   */
  protected $entity;

  /**
   * @var string
   */
  protected $file;

  /**
   * @var bool
   */
  protected $update;

  /**
   * EntityImportEvent constructor.
   * @param ContentEntityInterface $entity
   * @param string $file
   * @param bool $update
   */
  public function __construct(ContentEntityInterface $entity, $file, $update = FALSE) {
    $this->entity = $entity;
    $this->file = $file;
    $this->update = $update;
  }


  /**
   * @return ContentEntityInterface
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * @param ContentEntityInterface $entity
   */
  public function setEntity(ContentEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * @return string
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * @param string $file
   */
  public function setFile($file) {
    $this->file = $file;
  }

  /**
   * @return boolean
   */
  public function isUpdate() {
    return $this->update;
  }

  /**
   * @param boolean $update
   */
  public function setUpdate($update) {
    $this->update = $update;
  }

}